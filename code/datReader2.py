import numpy as np
import re
import matplotlib.pyplot as pt
from scipy.optimize import curve_fit

# this class magages the information in a single column of a .dat file
class Initiator:
    @staticmethod
    def ensure(dic,**args):
        listVariables = []
        for (key, val) in args.items():
            if key in dic:
                if (type(dic[key]) != type(val)):
                    raise Exception("You inserted argument "+key+" with a value of the wrong type")
            else:
                dic[key]=val
            listVariables.append(dic[key])
        return listVariables

class Data:
    unit = ""
    values = np.array([])
    name = ""
    lable = ""

    def getArangedValues(self,starVal=-1, interval=1.0):
        return np.arange(starVal,len(self.values)-1,interval)

    def getDefaultPlot(self,**arg):
        xLabel, plt = Initiator().ensure(arg,
                       xLabel="X",
                       plt=pt
                       )
        #plt=arg["plt"]
        plt = pt
        plt.plot(self.getArangedValues(),self.values,label=self.lable)
        plt.ylabel(self.lable)
        plt.xlabel(arg["xLabel"])
        return plt

    def __init__(self, values, unit, name):
        self.unit = unit
        self.values = values
        self.name = name
        self.lable = name+" ["+unit+"]"

# this is a dictionary of type String:Data
class DatReader(dict):

    # ***** START METHODS REQUIRED FOR OVERRIDING DICTIONARY TYPE ******
    def __init__(self,fileName, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys
        self.__openFile(fileName)

    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]
    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = value
    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]
    def __iter__(self):
        return iter(self.store)
    def __len__(self):
        return len(self.store)
    def __keytransform__(self, key):
        return key
    # ***** END ******

    def __openFile(self,fileName):
        matrix = self.__fillMatrix(fileName)
        matrix = self.__swapMatrix(matrix)
        self.__createData(matrix)
        #self.data = self.__createData(matrix)

    def __fillMatrix(self,fileName):
        matrix = []
        protoContent = [line.rstrip('\n') for line in open(fileName)]
        self.protoContent = protoContent
        #self.matrix = [str.split(' ', 1 ) for line in protoContent]
        for line in protoContent:
            result = []
            split = line.split("\t",1)
            if len(split) == 1:
                split = line.split("  ",1)
            for element in split:
                element = element.replace("\r","").replace("\n","").replace(",",".")
                if element != "":
                    result.append(element)
            matrix.append(result)
        return matrix

    def __swapMatrix(self,matrix):
        result = []
        for index in range(len(matrix[1])):
            column = []
            for vector in matrix:
                column.append(vector[index])
            result.append(column)
        return result

    def __getLabel(self,item):
        result = {}
        result["unit"] = re.search(r"\((\w+)\)", item).group(1)
        result["dataName"] = re.search('((\w+)\s)', item).group(1).replace(" ","")
        return result

    def __fixData(self,dataArray):
        result = np.array([])
        for item in dataArray:
            result = np.append(result, np.float(item))
        #print(result)
        return result

    def __createData(self,matrix):
        #data = {}
        for vector in matrix:
            label = self.__getLabel(vector[0])
            #print(vector)
            vector.pop(0)
            #print(vector)
            self[label["dataName"]] = Data(self.__fixData(vector),label["unit"],label["dataName"])
        #return data
