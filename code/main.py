import time
from datReader2 import DatReader
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit


dataHumidity = DatReader("humidity.dat")
dataTemperature = DatReader("temperature.dat")
dataOpenTime = DatReader("timeOpen.dat")

INTERVAL = 15
BASE_LIMIT_TRESHOLD = 0.001
probabilities = []
week = []
month = []
year = []
partialSum = []
for i in range((96)):
    probabilities.append(0.0)
    week.append(0.0)
    month.append(0.0)
    year.append(0.0)
    partialSum.append(0)

openTime = 0.0
curredtDay = 1
limitTreshold = 0.0
WISHEDTEMPERTURE = 4.0 
tresholds=[]

def highTreshold():
    tresholds.append(1.5)
def lowTreshold():
    tresholds.append(0)

def updateThreshold(probability):
    if probability>limitTreshold:
        lowTreshold()
        #go to active
    else:
        highTreshold()
def getOpenTime(i):
    return dataOpenTime["openduration"].values[i]
def getTemperature(i):
    return dataTemperature["externalTemp"].values[i]
def getHumidity(i):
    return dataHumidity["humidity"].values[i]

def getLimitTreshold(externalTemp,humidity):
    return float(BASE_LIMIT_TRESHOLD)-0.00*((humidity + (float(externalTemp) - WISHEDTEMPERTURE)/(40.0-WISHEDTEMPERTURE))-1.0)

def updateProbability(i, openTime):
    print("probabilities : " + str(probabilities[i]) + "  openTime: " + str(openTime) + " partialSum: " + str(openTime))
    if(curredtDay % 7 == 0):
	partialSum[i] = openTime
	week[i] = partialSum[i]/(INTERVAL)
	month[i] = (month[i] + week[i])/2
    else:
	partialSum[i]+=openTime
        week[i] = (partialSum[i])/(((curredtDay%7))*INTERVAL)
    if (curredtDay % 30 == 0):
        year[i] = 0.6*year[i]+0.4*month[i]
    probabilities[i] = 0.6 * week[i] + 0.3 * month[i] + 0.1 * year[i]


k = 0;
while True:
    for i in range(0, len(probabilities)):
	print(i)
	print(k)
        limitTreshold = getLimitTreshold(getTemperature(i),getHumidity(i))
        print("giorno: " +str(curredtDay)+" probability: " +str(probabilities[i]) +" limitThreshold:  "+ str(limitTreshold))
        updateThreshold(probabilities[i])
        #time.sleep(INTERVAL)
        openTime = getOpenTime(k)
        updateProbability(i,openTime)
	k+=1
    curredtDay += 1
    if curredtDay == 8:
        break

#plt.title("Radius squared of Kth Newton's ring")

print("a: " +str(len(tresholds)))
print("b: " +str(len(dataOpenTime["openduration"].values)))
plt.plot(range(0,len(tresholds)),tresholds,'ro')
conc = np.concatenate([dataOpenTime["openduration"].values])
plt.plot(range(0,len(tresholds)),conc,'b-')
#plt.ylim(0,1.5)
#plt.plot(xs,linear(xs,m,c),"--", label="linear fit")
#plt.legend(loc=4)
plt.show()
